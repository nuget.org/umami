[[_TOC_]]
## Incident Report for a known / recuring situation
<!-- Use if the incident is a recurring issue, with an already validated change procedure -->
<!-- Add reference to already validated related incident or Emergency change with /relate in the comments -->
/relate nomadic-labs/umami-wallet/umami#xyz 


<!-- (optional) To bring the attention to this incident during next CAB meeting, uncomment next line to add CAB::to-review label -->
<!-- /label ~Change ~"CAB::to-review" -->

### What is the nature of the ~incident?
<!-- Which parts of the system where affected ? -->


### How the incident was discovered?
<!-- Mention which alert was triggered where, but report wrong or missing alerts when it's the case -->
<!-- slack ? manual check ? user notification ? other -->


### What is the potential impact of the incident?


### What is the evidence (i.e. screenshots, logs, etc)?



<!-- METADATA for project management, please leave the following lines and edit as needed -->
# Metadata
<!-- Severity : pick one the gitlab panel, right side of the window when viewing the incident after creation -->

/label ~incident ~"incident::known"
<!-- Labels and default review status for gitlab Change management process, comment if no change was performed-->
/label ~Change ~"Change::Emergency"

## Reviews checklist (none required)
<!-- No review required since this is a known incident with already validated change procedure to be linked -->

<!-- (optional) To bring the attention to this incident during next CAB meeting, add CAB::to-review label -->
<!-- /label ~Change ~"CAB::to-review" -->


<!-- METADATA - end -->

<!-- Other useful shortcuts -->
<!-- ( ping CAB members : @picdc @remyzorg @comeh @philippewang.info @SamREye ) -->
<!-- Trigger gitlab todo tasks : mention @user at the start of the line --> 
<!-- @remyzorg (cc: TBD )    Please review this _emergency change_ on development aspects -->
<!-- @comeh (cc: @philippewang.info) Please review this _emergency change_ on operations aspects  -->
<!-- @SamREye                   Please review this _emergency change_ on business aspects    -->
<!-- /assign @premyzorgicdc @comeh @SamREye -->
<!-- /unlabel ~"CAB::to-review" -->
