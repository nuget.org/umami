[[_TOC_]]
## Regular change
<!-- Regular change, to be approved by the CAB before applying. -->

<!-- /confidential -->
<!-- If confidential, explain why -->

### Summary
<!-- Outline the issue being faced, and why this required a change !-->

### Area of the system
<!-- This might only be one part, but may involve multiple sections !-->

### How does this currently work?
<!-- The current process, and any associated business rules !-->

### What is the desired way of working?
<!-- After the change, what should the process be, and what should the business rules be !-->

<!-- Success criteria of change application (when relevant, include how to test) -->

### Change Procedure
- [ ] Change procedure been tested successfully

<!-- Include step by step description -->


## Rollback plan
<!-- Describe how to rollback the change in case the expected change is not working -->


<!-- METADATA for project management, please leave the following lines and edit as needed -->
# Metadata
<!-- PRIORITY: Uncomment /label quick actions as appropriate. The priority and severity assigned may be different to this !-->
<!--High : (This will bring a huge increase in performance/productivity/usability, or is a legislative requirement)-->
<!-- /label ~"Priority::1-High" -->
<!--Medium : (This will bring a good increase in performance/productivity/usability)-->
<!-- /label ~"Priority::2-Medium" -->
<!--Low : (anything else e.g., trivial, minor improvements) -->
<!--  /label ~"Priority::3-Low" -->

## Approvals checklist (all required) 
- [ ] Approval from Development
- [ ] Approval from Operations
- [ ] Approval from Business
<!-- tick the corresponding checkbox [x], you may also add your @user handle at the end of the line -->

<!-- Trigger gitlab todo tasks --> 

@remyzorg (cc: TBD )    Please *approve* this _regular change_ on development aspects

@comeh (cc: @philippewang.info) Please *approve* this _regular change_ on operations  aspects

@SamREye                   Please *approve* this _regular change_ on business    aspects

<!-- /assign @remyzorg @comeh @SamREye -->

<!-- Quick actions for last approver : -->
<!-- /unlabel ~"CAB::to-approve" -->
<!-- /label ~"CAB::to-perform"   -->

/label ~"Change::Regular" <!-- Regular change, to be approved by the CAB before applying. -->
/label ~Change ~"CAB::to-approve" <!-- labels for gitlab CAB Change issues management -->
<!-- METADATA - end -->
